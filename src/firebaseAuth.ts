import Firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';
import { observable, computed, action, toJS } from 'mobx'
import { Alert } from 'react-native'

class FirebaseClass {

    signUp (email:string, password:string) {
        return new Promise((resolve, reject) => {
             Firebase.auth()
            .createUserWithEmailAndPassword(email, password)
            .then(() => resolve ({
                "success":true
            }))
            .catch(error => resolve({
                "success":false,
                "message":error
            }))
        })
    }
    
    login(email:string, password:string) {
        return new Promise((resolve, reject) => {
            Firebase.auth().signInWithEmailAndPassword(email, password).then((response) => {
                resolve(response)
            }).catch((error) => { 
                reject(error) });
        })
    }

    logOut() {
        return Firebase.auth().signOut().then(function () {
            console.log('Log out OK')
            //removeUser().then(() => NavigatorService.navigate('Auth'));
            return true
        }).catch(function (error:any) {
            console.log('Log out ERROR')
            return false
        });
    }



}
let firebaseClass = new FirebaseClass()
export default firebaseClass 