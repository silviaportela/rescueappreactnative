

import { Component, ComponentType } from "react";
import React from 'react';
import {
    View,
    TouchableOpacity,
    Alert,
    StatusBar,
    Platform,
    Text, StyleSheet
} from 'react-native';

//Navigators
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
import { Icon } from 'react-native-elements';
import colors from '../style/colors'
import strings from '../style/strings'
import navigatorService from '../navigation/navigatorService';



var convert = require('color-convert');

export interface Props {
    backButton: boolean, //obligatorio
    headerTitle: string, //obligatorio
    logoutButton: boolean //obligatorio
    secondLeftIconName?: string, //optional 
    secondLeftIconAction?: Function, //optional
    secondRightIconName?: string, //optional
    secondRightIconAction?: Function, //optional
    color?: boolean,
}

const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

function StatusBarPlaceHolder() {
    var color = convert.cmyk.hex(convert.hex.cmyk(colors.primaryColor));
    return (
        Platform.OS === 'ios' ?
            <View style={{
                width: "100%",
                height: STATUS_BAR_HEIGHT,
                backgroundColor: colors.primaryColor
            }}>
                <StatusBar
                    barStyle="light-content"
                />
            </View>
            : <StatusBar
                backgroundColor={colors.primaryColor}
                barStyle="light-content"
            />
    );
}

export default class CustomHeader extends React.Component {

    _logoutAlert() {
        console.log("logour")
    }
    render() {
        return (
            <View>
                <StatusBarPlaceHolder />
                <View style={{
                    width: responsiveWidth(100),
                    height: responsiveHeight(8),
                    flexDirection: 'row',
                    backgroundColor: this.props.color ? colors.secondaryBlueColor : colors.primaryColor,
                    alignItems: 'center',
                    elevation: 1,
                    justifyContent: 'space-between'
                }}>
                    <View style={{ flexDirection: 'row' }}>

                        {this.props.backButton &&
                            <Icon containerStyle={styles.iconsContainer}
                                name={this.props.backButton ? 'arrow-back' : 'menu'}
                                type='material'
                                size={responsiveHeight(4)}
                                color={'white'}
                                onPress={() => navigatorService.back()} />
                        }
                         {this.props.secondLeftIconName && this.props.secondLeftIconAction &&
                                    <Icon
                                        containerStyle={styles.iconsContainer}
                                        name={this.props.secondLeftIconName}
                                        onPress={() => this.props.secondLeftIconAction()}
                                        type='material'
                                        size={responsiveHeight(4)}
                                        color={'white'} />
                                }



                    </View>
                    <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={styles.headerTitleText}>{this.props.headerTitle}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }} >
                        {this.props.logoutButton &&
                            <Icon containerStyle={styles.iconsContainer}
                                name='exit-to-app'
                                onPress={() => this._logoutAlert()}
                                type='material'
                                size={responsiveHeight(4)}
                                color={'white'} />
                        }
                        {this.props.secondRightIconName && this.props.secondRightIconAction &&
                            <Icon
                                containerStyle={styles.iconsContainer}
                                name={this.props.secondRightIconName}
                                onPress={() => this.props.secondRightIconAction()}
                                type='material'
                                size={responsiveHeight(4)}
                                color={'white'} />
                        }
                    </View>
                </View>
            </View>

        );
    }
}
const styles = StyleSheet.create({
    iconsContainer: {
        padding: 2,
    },
    iconsContainerRigth: {
        padding: 2,
        alignSelf: 'flex-end'
    },
    blue_button: {
        width: responsiveWidth(80),
        height: responsiveHeight(6),
        justifyContent: 'center',
        backgroundColor: colors.primaryColor,
        shadowOpacity: 0.8,
        shadowRadius: 4,
        borderRadius: 6,
        alignItems: 'center',
    },

    blue_button_text: {
        color: 'white',
        fontSize: responsiveFontSize(2),
        fontWeight: 'bold',
    },

    register_device_text: {
        color: colors.primaryColor,
        fontSize: responsiveFontSize(2),
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 5
    },

    headerTitleText: {
        color: 'white',
        fontSize: responsiveFontSize(2),
        fontWeight: 'bold',
    },

    listItem: {
        height: responsiveHeight(10),
        backgroundColor: '#F6F6F6',
        width: responsiveWidth(95),
    },

    clearTitle: {
        color: colors.thirdBlueColor,
        fontWeight: 'bold',
        marginTop: responsiveHeight(2),
        marginLeft: responsiveHeight(2),
        fontSize: responsiveFontSize(2)
    },

    blueInput: {
        color: colors.primaryColor,
        fontWeight: 'bold',
        margin: responsiveHeight(2),
        fontSize: responsiveFontSize(2)
    },
    footer: {
        height: 100,
        width: responsiveWidth(100),
        position: 'absolute',
        justifyContent: 'space-between',
        bottom: 20,
        flexDirection: 'row',
    },
    bottomRightFloatingButton: {
        position: 'absolute',
        bottom: 20,
        right: 20
    },
    bottomRightFloatingButtonModal: {
        position: 'absolute',
        bottom: 80,
        right: 20
    }
})
