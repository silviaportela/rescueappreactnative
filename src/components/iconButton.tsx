import React from 'react';
import colors from '../style/colors'
import { StyleSheet} from 'react-native';
import { Icon } from 'react-native-elements';

export interface Props {
    iconName: string, //obligatorio
    disabled?:boolean,//opcional
    iconAction: Function, //obligatorio
}


export default class IconButton extends React.Component<Props>{

    render() {
        return (

            <Icon
                containerStyle={customStyle.bottomRightFloatingButton }
                reverse
                raised
                color='red' 
                // color={this.props.disabled ? 'red' : colors.secondaryColor}
                name={this.props.iconName}
                type='font-awesome'
                reverseColor='white'
                disabled={this.props.disabled ? true : false}
                onPress={() => this.props.iconAction()}

            />

        )
    }
}

const customStyle = StyleSheet.create({

    bottomRightFloatingButton: {
      position: 'absolute',
      bottom: 20,
      right: 20
    },
  });
  
