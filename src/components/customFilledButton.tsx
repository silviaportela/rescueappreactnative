import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity ,Text} from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';


export interface Props {
    onPress: Function,
    buttonColor: string,
    textColor: string,
    title:string
}


export default class CustomFilledButton extends React.Component<Props> {

    render() {
        return (
            <TouchableOpacity
                style={[styles.button, { backgroundColor: this.props.buttonColor }]}
                onPress={() => this.props.onPress()} >
                <Text style={[styles.button_text, { color: this.props.textColor }]}>{this.props.title}</Text>
            </TouchableOpacity >
        )
    }

}

const styles = StyleSheet.create({
    button: {
        width: responsiveWidth(85),
        height: responsiveHeight(8),
        justifyContent: 'center',
        borderRadius: 6,
        alignSelf: 'center',
        alignItems:'center'
    },
    button_text: {
        fontSize: responsiveFontSize(2),
        fontWeight: 'bold',
    },

});