
export const hosts = {
   // dev: 'http://10.0.2.2:3000/'
    //dev:'http://192.168.1.68:3000/'
    dev:'http://192.168.43.132:3000/'
   //dev:'http://192.168.0.24:3000/'
    
}

export const endPoints = {
    getAlerts:'alerts/',
    getMyAlerts:'alerts/userAlerts/',
    getNoAlerts:'alerts/userNoAlerts/',
    getUsers:'user/',
    getTracks:'tracks/',
    getMessages:'messages/',
    getZones:'zonas/',
    joinTo:'alerts/jointo/',
    createUser:'user/createUser'
}
