
import { hosts, endPoints } from './config';
import { fetch_Auth_POST, fetch_GET_get_data,fetch_Auth_PUT } from './utils'
import { useReducer } from 'react';
import { YellowBox } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

class Platform {

    private host: string;
    constructor() {
        this.host = hosts.dev;
        YellowBox.ignoreWarnings(['ViewPagerAndroid']);YellowBox.ignoreWarnings(['Warning: Async Storage has been extracted from react-native core']);
    }

    async createUser(name: String) {
        try {
            const response = await fetch_Auth_POST(`${this.host}${endPoints.createUser}`, {
               "name":name
            });
            console.log("createUser", response)
            return response;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    }

    //*************Alerts************//
    async getAlerts() {
        try {
            const response = await fetch_GET_get_data(`${this.host}${endPoints.getAlerts}`);
            return response;
        }
        catch (error) {
            return 333;
        }
    }

    async getMyAlerts(user:String) {
        try {
            const response = await fetch_GET_get_data(`${this.host}${endPoints.getMyAlerts}`+user);
            return response;
        }
        catch (error) {
            return 333;
        }
    }

    async getNoAlerts(user:String) {
        try {
            const response = await fetch_GET_get_data(`${this.host}${endPoints.getNoAlerts}`+user);
            console.log("response",response)
            return response;
        }
        catch (error) {
            return 333;
        }
    }

    async getAlertById(id: String) {
        try {
            const response = await fetch_GET_get_data(`${this.host}${endPoints.getAlerts}` + id);
            return response;
        }
        catch (error) {
            return 333;
        }
    }

    async joinToAlert(user:String,alertName:String) {
        try {
            const response = await fetch_Auth_PUT(`${this.host}${endPoints.joinTo}`+alertName, {
                "user":user
            });
            return response;
        }
        catch (error) {
            return 333;
        }
    }

    //*************Tracks************//
    async getTracks() {
        try {
            const response = await fetch_GET_get_data(`${this.host}${endPoints.getTracks}`);
            return response;
        }
        catch (error) {
            return 333;
        }
    }

    async getTracksByAlertId(id: String) {
        try {
            const response = await fetch_GET_get_data(`${this.host}${endPoints.getTracks}` + id);
            return response;
        }
        catch (error) {
            return 333;
        }
    }

    async setTrack(idAlert: String, coordinates: any) {
        try {
            const response = await fetch_Auth_POST(`${this.host}${endPoints.getTracks}`, {
                "_id": "",
                "idAlert": idAlert,
                "coordinates": coordinates
            });
            console.log("espspspsps", response)
            return response;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    }


    //************* Messages ************//
    async getMessagesById(id: String) {
        try {
            const response = await fetch_GET_get_data(`${this.host}${endPoints.getMessages}` + id);
            return response;
        }
        catch (error) {
            return 333;
        }
    }

    async setMessage(data: any) {
        try {
            const response = await fetch_Auth_POST(`${this.host}${endPoints.getMessages}`, {
                _id: "",
                user:data.user,
                idAlert: data.idAlert,
                timestamp: data.timestamp,
                message: data.message
            });
            console.log("setMessage", response)
            return response;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    }


    //*******************ZONES*****************//


  async getZonesByAlertId(id: String) {
        try {
            const response = await fetch_GET_get_data(`${this.host}${endPoints.getZones}` + id);
            console.log("GET ZONES",response)
            return response;
        }
        catch (error) {
            return 333;
        }
    }


//async

async getUserProfile() {
    console.log();
    const result = await AsyncStorage.getItem("USERNAME");
    console.log("Async GET USER USERNAME",result)
    var obj = JSON.parse(result);       
    return obj;
}
setUserProfile(profile: string) {
    console.log('Async SET USER USERNAME',JSON.stringify(profile));
    return AsyncStorage.setItem("USERNAME", JSON.stringify(profile)).then(() => {
        return true
    }).catch(() => {
        return false
    });
}

resetUserProfile = async () => {
    return await AsyncStorage.removeItem("USERNAME");
}

}
let platform = new Platform()
export default platform;