export function fetch_Auth_POST(url: string, body: object) {      
   // console.log("BODY DE fetch_Auth_POST body:", JSON.stringify(body))
    console.log("BODY DE fetch_Auth_POST url:", url)
    return fetch(url, {
        method: 'POST',
        body: JSON.stringify(body),
        headers:{
            'Content-Type': 'application/json'
          }
    }).then((response) => {
        if (response.status === 201) {
            return response.json();
        } else {
            return response.status;
        }
    });
}


export function fetch_GET_get_data(url: string) {
    console.log("BODY DE fetch_GET_get_data url:", url)
    return fetch(url, {
        method: 'GET',
    }).then((response) => {
        console.log("fetch_GET_get_data", response.status)
        if (response.status === 200) {
            return response.json();
        } else {
            return response.status;
        }
    });
}

// export function fetch_Auth_DELETE(url: string, body: object, auth: string) {
//     return fetch(url, {
//         method: 'DELETE',
//         headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + auth, },
//         body: JSON.stringify(body),
//     }).then((response) => {
//         if (response.status === 200) {
//             return response.json();
//         } else {
//             return response.status;
//         }
//     });
// }

export function fetch_Auth_PUT(url: string, body: object, auth: string) {
    return fetch(url, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + auth, },
        body: JSON.stringify(body),
    }).then((response) => {
        if (response.status === 200) {
            return response.json();
        } else {
            return response.status;
        }
    });
}



