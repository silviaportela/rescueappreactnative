
import LocalizedStrings from 'react-native-localization';

let strings = new LocalizedStrings({
     en: {
        createAccount: ' SIGN IN',
        signIn: 'LOG IN',
        email_placeHolder:'email',
        password_placeHolder:'password',
        register_user_header_title:'SIGN UP',
        password_placeHolder_confirmation:'confirm password',
        logout_msg: "Are you sure?"

    },
    eu:{
        createAccount: 'KONTUA CREATU',
        signIn: 'SARTU',
        email_placeHolder:'email',
        password_placeHolder:'pasahitza',
        register_user_header_title:'erregistratu',
        password_placeHolder_confirmation:'pasahitza confirmatu',
        logout_msg:"¿Estás segura/o?"
    }
})

export default strings;


