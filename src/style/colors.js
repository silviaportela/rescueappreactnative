module.exports = {
    listTitleColor: 'black',
    listSubtitleColor: 'gray',
    primaryColor: '#2a78f7',
    primaryColorSoft: '#2a78f7aa',
    secondaryColor:'#ffb43b',
    secondaryBlueColor: '#6babf9',
    thirdBlueColor: '#90c0f9',
    statusBarDarkPrimary:'#0853cd',
    thirdBlueColor_: '#90c0f9ee',
    listItemColor:'#f1f9ff',//'#c6e3f4',//
    dark:'#666666',
}
