import { observable, action, computed,toJS } from 'mobx';
import { getMyAlerts,getNoAlerts, getAlertById, joinToAlert } from '../dataInterface/cloudDataInterface';
import { PermissionsAndroid, Alert } from 'react-native';

export class MAlerts {
    constructor(props) {
        this.requestCameraPermission();
    }

    @observable _alerts:any = []
    @observable _noAlerts:any = []
    @observable _alertIDSelected:string = ""
    @observable _alertSelected:any = []
    @observable _permission:boolean=false;
    @observable _myAlert:boolean=false;

    @action setAlerts(value:any) {
        this._alerts = value;
    }
    @action setNoAlerts(value:any) {
        this._noAlerts = value;
    }
    @action setAlertIDSelected(value:string) {
        this._alertIDSelected = value
    }
    @action setAlertSelected(value:any) {
        this._alertSelected = value
    }
    @action setPermission(value:boolean) {
        this._permission = value
    }
    @action setMyAlert(value:boolean) {
        this._myAlert = value
    }
    @computed get alertIDSelected() {
        return this._alertIDSelected;
    }
    @computed get alertSelected() {
        return this._alertSelected;
    }
    @computed get alerts() {
        return this._alerts;
    }
    @computed get noAlerts() {
        return this._noAlerts.slice();
    }
    @computed get permission() {
        return this._permission;
    }
    @computed get myAlert() {
        return this._myAlert;
    }

    getAlerts(userName:String) {
        return getMyAlerts(userName).then((response) => {
            if (response == 500 || response == 400 || response == 333) {
                return false;
            } else {
                this._alerts = response.json;
                console.log("this._alerts",this._alerts)
                return true;
            }
        });
    }

    getNoAlerts(userName:String) {
        return getNoAlerts(userName).then((response) => {
            if (response == 500 || response == 400 || response == 333) {
                return false;
            } else {
                this._noAlerts=response.json;
                console.log("this._noAlerts-->",toJS(this._noAlerts))
                return true;
            }
        });
    }

    joinTo(alert:any,userName:String)
    {
        return joinToAlert(userName,alert.name).then((response)=>{
            console.log("alert.name",alert.name)
            console.log("username",userName)
            console.log("response",response)
            this._alerts.push(alert) 
            var index = this._noAlerts.findIndex(p => p._id == alert._id)
            this._noAlerts.splice(index, 1)
            Alert.alert("Te has suscrito a la alerta "+ alert.name)
        })
    }

    getAlertById() {
        return getAlertById(this._alertIDSelected).then((response) => {
            if (response == 500 || response == 400 || response == 333) {
                return false;
            } else {
                this._alertSelected = response.json;
                return true;
            }
        });
    }



    async requestCameraPermission() {
        try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
              {
                'title': 'Location Permission',
                'message': 'This app needs access to your location',
              }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              this._permission=true;
            } else {
                this._permission=false;
            }
          } catch (err) {
            console.warn(err)
          }
        }

        meterEnArray(data:any){
            this._noAlerts.push(data);
        }

}