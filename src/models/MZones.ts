import { observable, action, computed,toJS } from 'mobx';
import { getZonesByAlertId } from '../dataInterface/cloudDataInterface';

export class MZones {
    constructor() {
        
    }

    @observable _zonesFromAlert:any[] = []

    @action setZonesFromAlert(value:any[]) {
        this._zonesFromAlert = value;
    }
    @computed get zonesFromAlert() {
        return this._zonesFromAlert;
    }
    

    getZonesByAlertId(alertID:String) {
        return getZonesByAlertId(alertID).then((response) => {
            if (response == 500 || response == 400 || response == 333) {
                return false;
            } else {
                this._zonesFromAlert = response.json;
                return true;
            }
        });
    }

   

}