import { observable, action, computed } from "mobx";
import Sockets from 'react-native-sockets';
import { DeviceEventEmitter, Alert, ToastAndroid } from 'react-native';
import io from "socket.io-client";
import { instance } from '../models/index'
import { YellowBox } from 'react-native'


import NotificationService from '../navigation/notificationService'

var notification:NotificationService
export class MSocket {

    constructor() {
        notification = new NotificationService(this.onNotification);
        console.log("thisnotification-->",notification)
        this.initListeners();
        YellowBox.ignoreWarnings([
            'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?'
          ])
      

    }
    @observable _socket = io("http://192.168.43.132:2222");
    //@observable _socket = io("http://192.168.0.24:2222");
    //@observable _socket = io("http://10.18.48.53:2222");
    @observable _connected:boolean = false;

    @action setSocket(value) {
        this._socket = value;
    }
    @computed get socket() {
        return this._socket;
    }
    @action setConnected(value:boolean) {
        this._connected = value;
    }
    @computed get connected() {
        return this._connected;
    }

    onNotification = (notif) => {
        Alert.alert(notif.title, notif.message);
      }

        //Permissions to use notifications
      handlePerm(perms) {
        Alert.alert("Permissions", JSON.stringify(perms));
      }


    initListeners() {
        this._socket.on("connect", function () {
            ToastAndroid.show("Conectado al servidor en el puerto 2222", ToastAndroid.SHORT);
        });

        this._socket.on("mensaje", function (data:any) {
            console.log("Servidor: " + data)
            ToastAndroid.show("Servidor: " + data, ToastAndroid.SHORT);
          
        });

        this._socket.on("broadcast", function (data:any) {
            console.log("**********");
            console.log("He recibido mensaje de " + data.user);
            console.log("Para la alerta " + data.idAlert);
            console.log("A dia de " + data.timestamp);
            console.log("Que dice: " + data.message);
            console.log("**********");
            instance().MSocket.meterEnArray(data);
        });

        this._socket.on("notification", function (data:any) {
            console.log("notification: ",data)
            //ToastAndroid.show("notification: " + data, ToastAndroid.SHORT);
            instance().MAlerts.meterEnArray(data);
            notification.localNotification()
         
        });

        this._socket.on("disconnect", function () {
            ToastAndroid.show("Desconectado: ",10);
        });


    }

    // onReceivedMessage(messages) {
    //     this._storeMessages(messages);
    // }
    // onSend(messages = []) {
    //     this._storeMessages(messages);
    // }
    initSocket() {
        this._socket.emit('mensaje', "cheeee")

    }

    sendMensaje(data) {
        this._socket.emit('mensaje From client', data);
    }

    disconnect() {
        Sockets.disconnect();
    }

    meterEnArray(data:any) {
        console.log("Estoy en esa alerta?" + data.idAlert == instance().MAlerts.alertIDSelected)

        if (data.idAlert == instance().MAlerts.alertIDSelected && data.user == instance().MLogin.username)
            ToastAndroid.show("Es la alerta en la que estoy y soy yo", ToastAndroid.SHORT);

        if (data.idAlert == instance().MAlerts.alertIDSelected && data.user != instance().MLogin.username)
            {
                ToastAndroid.show("Es la alerta en la que estoy y no soy yo", ToastAndroid.SHORT);
                instance().MMessages.addMessage(data)
            }

        if (data.idAlert != instance().MAlerts.alertIDSelected){
            console.log("MODELO",instance().MAlerts.alertIDSelected)
            console.log("DATA",data.idAlert )
            ToastAndroid.show("No es sobre mi alerta", ToastAndroid.SHORT);
        }
            
            
    }







}