import { observable } from 'mobx';

import {MSocket} from './MSocket';
import {Mlogin} from './Mlogin';
import {MAlerts} from './MAlerts';
import {MTracks} from './MTracks';
import {MMessages} from './MMessages';
import {MZones} from './MZones';


export class app{
    @observable MLogin:Mlogin=new Mlogin();
    @observable MSocket=new MSocket();
    @observable MAlerts=new MAlerts();
    @observable MTracks=new MTracks();
    @observable MMessages=new MMessages();
    @observable MZones = new MZones();
};