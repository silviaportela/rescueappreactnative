import { observable, action, computed } from 'mobx';
import navigatorService from '../navigation/navigatorService';
import FirebaseAuth from '../firebaseAuth'
import { Alert } from 'react-native';
import { createUser } from '../dataInterface/cloudDataInterface';
import dataInterface from '../dataInterface';

export class Mlogin {

    @observable _username = "";
    @observable _password = "";
    @observable _passwordConfirm= "";

    @observable _loginFormVisible = false;
    @observable _registerFormVisible = false;


    @observable _isLoggedIn= false;
    @observable _isLoading = false;
    @observable _confirmCodeVisible = false;
    @observable _error = "";
    @observable _token = "";
    @observable _register = false;
    @observable _loginForm = false;
    @observable _registerForm = false;
    

    @action setUsername(value) {
        this._username = value;
    }
    @computed get username() {
        return this._username;
    }
    @action onChangeUsername = (text) => {
        this._username = text;
        this._error = '';
    }

    @action setPassword(value) {
        this._password = value;
    }
    @computed get password() {
        return this._password;
    }
    @action onChangePassword = (text) => {
        this._password = text;
        this._error = '';
    }

    @action setPasswordConfirm(value) {
        this._passwordConfirm = value;
    }
    @computed get passwordConfirm() {
        return this._passwordConfirm;
    }
    @action onChangePasswordConfirm = (text) => {
        this._passwordConfirm = text;
    }

    @action setIsLoggedIn(value) {
        this._isLoggedIn = value;
    } @computed get isLoggedIn() {
        return this._isLoggedIn;
    }


    @action setIsLoading(value) {
        this._isLoading = value;
    } @computed get isLoading() {
        return this._isLoading;
    }

    @action setIsAppReady(value) {
        this._isAppReady = value;
    }
    @computed get isAppReady() {
        return this._isAppReady;
    }

    @action setConfirmCodeVisible(value) {
        this._confirmCodeVisible = value;
    }
    @computed get confirmCodeVisible() {
        return this._confirmCodeVisible;
    }

    @action setError(value) {
        this._error = value;
    }
    @computed get error() {
        return this._error;
    }
    @action setToken(value) {
        this._token = value;
    }
    @computed get token() {
        return this._token;
    }
    @action setRegister(value) {
        this._register = value;
    }
    @computed get register() {
        return this._register;
    }
    @action setLoginForm(value) {
        this._loginForm = value;
    }
    @computed get loginForm() {
        return this._loginForm;
    }
    @action setRegisterForm(value:boolean) {
        this._registerForm = value;
    }
    @computed get registerForm() {
        return this._registerForm;
    }

    empty(){
        this._password = '';
        this._passwordConfirm = '';
        this._isLoggedIn=false;
    }

    login(){
        FirebaseAuth.login(this._username,this._password).then((response)=>{
            dataInterface.setUserProfile(this._username)
            navigatorService.navigate('Drawer')
        }).catch((error)=>{
            Alert.alert("error",error.message)
        })

    }

    signUp(){
        return FirebaseAuth.signUp(this._username,this._password).then((response:any)=>{
            if (response.success==true){
                Alert.alert("Usuario Creado");
                createUser(this._username).then((response)=>{
                   dataInterface.setUserProfile(this._username).then((response)=>{
                    navigatorService.navigate('Drawer')
                    return true;
                   });
                  
                })
            }
            else{
                Alert.alert("error",response.message.message)
                return false;
            }
        })
    }

    logOut(){
        this._username="";
        this._password="";
        this._passwordConfirm="";
        return FirebaseAuth.logOut();
        
    }    
}