import { observable, action, computed,toJS } from 'mobx';
import { getMessagesById, postMessage } from '../dataInterface/cloudDataInterface';

export class MMessages {

    @observable _messages = []
    @action setMessages(value) {
        this._messages = value;
    }
    @computed get messages() {
        return this._messages;
    }

    @observable _messagesYO = []
    @action setMessagesYO(value) {
        this._messagesYO = value;
    }
    @computed get messagesYO() {
        return this._messagesYO;
    }
  
    @observable _messagesOther = []
    @action setMessagesOther(value) {
        this._messagesOther = value;
    }
    @computed get messagesOther() {
        return this._messagesOther;
    }

    @observable _myMessage = ""
    @action setMyMessage(value) {
        this._myMessage = value;
    }
    @computed get myMessage() {
        return this._myMessage;
    }

    addMessage(data){
        this._messages.push(data)
    }
  

    repartirMessages(username){
        this._messagesYO=this._messages.filter(item =>item.user === username);
        this._messagesOther=this._messages.filter(item =>item.user != username);
        console.log("_messagesYO",toJS(this._messagesYO))
        console.log("_messagesOther",toJS(this._messagesOther))
    }
  
  
    getMessagesById(alertId) {
        return getMessagesById(alertId).then((response) => {
            if (response == 500 || response == 400 || response == 333) {
                return false;
            } else {
                this._messages = response.json;
                return true;
            }
        });
    }

    postMessage(data) {
        return postMessage(data).then((response) => {
            if (response == 500 || response == 400 || response == 333) {
                return false;
            } else {
                this._messages.push(data)
                this._myMessage="";
                return true;
            }
        });
    }
}