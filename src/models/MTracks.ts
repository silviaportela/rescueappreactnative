import { observable, action, computed,toJS } from 'mobx';
import { getTracks, getTracksByAlertId,setTrack } from '../dataInterface/cloudDataInterface';

export class MTracks {
    constructor(props:any) {
        
    }

    @observable _tracks:any = []

    @observable _myTrack:any = []
    @observable _tracking:boolean = false;
    @observable _latitude:String = "";
    @observable _longitude:String = "";

    @action setTracks(value:any) {
        this._tracks = value;
    }
    @computed get tracks() {
        return this._tracks;
    }
    @action setTracking(value:any) {
        this._tracking = value;
    }
    @computed get tracking() {
        return this._tracking;
    }

    
    @action setMyTrack(value:any) {
        this._myTrack = value;
    }
    @computed get myTrack() {
        return this._myTrack;
    }
    @action setLatitude(value:any) {
        this._latitude = value;
    }
    @computed get latitude() {
        return this._latitude;
    }
    @action setLongitude(value:any) {
        this._longitude = value;
    }
    @computed get longitude() {
        return this._longitude;
    }
   

    getTracks() {
        return getTracks().then((response) => {
            if (response == 500 || response == 400 || response == 333) {
                return false;
            } else {
                this._tracks = response.json;
                return true;
            }
        });
    }

    getTracksByAlertId(alertID:String) {
        return getTracksByAlertId(alertID).then((response) => {
            if (response == 500 || response == 400 || response == 333) {
                return false;
            } else {
                this._tracks = response.json;
                return true;
            }
        });
    }

    postTrack(id:String) {
        return setTrack(id,toJS(this._myTrack)).then((response) => {
            this._tracking=false;
            if (response == 500 || response == 400 || response == 333) {
                this._myTrack=[]
                return false;
            } else {
                var objeto:any={
                    "_id":response.mensaje,
                    "idAlert":id,
                    "coordinates":toJS(this._myTrack),   
                }
                this._tracks.push(objeto)
                this._myTrack=[]
                return true;
            }
        });
    }




}