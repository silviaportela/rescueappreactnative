import { api } from '../api'

export function getAlerts() {
    return api.getAlerts();
}

export function getMyAlerts(user:String) {
    return api.getMyAlerts(user);
}
export function getNoAlerts(user:String) {
    return api.getNoAlerts(user);
}

export function getAlertById(id:String) {
    return api.getAlertById(id);
}

export function getTracks() {
    return api.getTracks();
}

export function createUser(name:String) {
    return api.createUser(name);
}

export function setUserProfile(name:string) {
    return api.setUserProfile(name);
}

export function resetUserProfile() {
    return api.resetUserProfile();
}

export function getUserProfile() {
    return api.getUserProfile();
}

export function getTracksByAlertId(id:String) {
    return api.getTracksByAlertId(id);
}

export function setTrack(idAlert:String,coordinates:any) {
    return api.setTrack(idAlert,coordinates);
}

export function getMessagesById(id:String) {
    return api.getMessagesById(id);
}

export function postMessage(data:any) {
    return api.setMessage(data);
}

export function getZonesByAlertId(id:String) {
    return api.getZonesByAlertId(id);
}

export function joinToAlert(user:String,alertName:String)
{
    return api.joinToAlert(user,alertName);
}











