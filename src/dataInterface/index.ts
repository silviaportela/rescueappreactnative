
import { getUserProfile,setUserProfile,resetUserProfile } from './cloudDataInterface'
class DataInterface {
    getUserProfile() {
        return getUserProfile();
    }
    setUserProfile(name:string) {
        return setUserProfile(name)
    }

    resetUserProfile(){
        return resetUserProfile();
    }
   
}

let dataInterface = new DataInterface()
export default dataInterface;



