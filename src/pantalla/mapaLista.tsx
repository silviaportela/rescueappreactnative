import React from 'react';
import { StyleSheet, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import { Icon } from 'react-native-elements';
import { View, Text } from 'react-native-animatable'
import { instance } from '../models'
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { SwipeListView } from 'react-native-swipe-list-view';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions'

import NavigatorService from '../navigation/navigatorService';
import colors from '../style/colors';
import strings from '../style/strings';
import CustomHeader from '../components/customHeader';
import IconButton from '../components/iconButton';
import navigatorService from '../navigation/navigatorService';

@observer
class MapaLista extends React.Component {

    constructor(props) {
        super(props)
    }

    _goToAlert(id:String){
        instance().MAlerts.setAlertIDSelected(id)
        instance().MAlerts.setMyAlert(false)
        navigatorService.navigate('MyNoAlertSwitch')
    }

    keyExtractor = (item:any, index:number) => index.toString()

    _renderEmptyList() {
        return (
            <View style={{ flex: 1, backgroundColor: '#dddddd', alignItems: 'center', paddingTop: responsiveHeight(10) }}>
                <Text style={{ marginTop: responsiveHeight(10), color: '#666666', fontSize: responsiveFontSize(2.5), textAlign: 'center', }}>
                    <Text style={{ fontSize: responsiveFontSize(3), fontWeight: 'bold' }}>No tienes ninguna{"\n"}alerta{"\n"}{"\n"}</Text>
                    Por favor, pulsa en el + para{"\n"}añadir tu primera alerta{"\n"}{"\n"}
                </Text>
            </View>
        )
    }

    _renderItemList(item:any) {
        return (
        <TouchableWithoutFeedback onPress={() => this._goToAlert(item.item._id)}>
                <View style={{ flexDirection: 'column', backgroundColor: colors.listItemColor, borderWidth: 1, borderColor: colors.listItemColor, margin: 5, borderRadius: 10, alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', paddingBottom: 15, paddingTop: 25, paddingHorizontal: 8, flex: 1 }}>
                        <View style={{ flex: 6, flexDirection: 'row' }}>
                            <Icon reverse name="map-marker" type='font-awesome' size={20} color={colors.primaryColor} />
                            <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 8 }}>
                                <Text style={{ fontSize: responsiveFontSize(2.5), color: colors.primaryColor, fontWeight: 'bold' }}>{item.item.name}</Text>
                                <Text style={{ fontSize: responsiveFontSize(2), color: colors.primaryColor }}>{item.item.city}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent:'flex-end' ,borderTopWidth: 1, borderColor: colors.primaryColor, width: '90%', padding: 8 }}>
                        {/* <Icon name="user" type='font-awesome' size={15} color={colors.primaryColor} /> */}
                        <Text style={{ fontSize: responsiveFontSize(1.5), color: colors.primaryColor, marginLeft:8 }}>Created by {item.item.user}</Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>

        )
    }

    _renderList() {

        let array = toJS(instance().MAlerts.noAlerts);
        return (
            <View style={{ flex: 1 }}>

                <SwipeListView
                    keyExtractor={this.keyExtractor}
                    data={array}
                    renderItem={(data, rowMap) => (this._renderItemList(data))}
                    renderHiddenItem={(data, rowMap) => (
                        <View style={styles.swipeBack} >
                            <View style={styles.swipeBack2} >
                                <View style={styles.swipeLeft}>
                                    {/* <TouchableOpacity style={[styles.swipeMenu, { alignItems: 'center' }]}>
                                        <Icon name="wifi-tethering" type='material' size={20} color='white' />
                                        <Text style={{ color: 'white', fontSize: responsiveFontSize(1.5) }}>Activar</Text>
                                        <Text style={{ color: 'white', fontSize: responsiveFontSize(2) }}>Cercanía</Text>
                                    </TouchableOpacity> */}
                                </View>
                                <View style={styles.swipeRight}>
                                    <TouchableOpacity style={styles.swipeMenu} onPress={() => instance().MAlerts.joinTo(data.item,instance().MLogin.username)}>
                                        <Text>Join</Text>
                                        <Icon name="hand-o-up" type='font-awesome' size={20} color='white' />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    )}
                    leftOpenValue={0}
                    rightOpenValue={-75}
                    stopLeftSwipe={responsiveWidth(1)}
                    stopRightSwipe={0 - responsiveWidth(45)}
                />
            </View>
        )
    }


    render() {
        console.log("this._noalertitas",toJS(instance().MAlerts.noAlerts))
        return (
            <View style={{ flex: 1 }}>
            <CustomHeader backButton={false} headerTitle="Rescue App" logoutButton={false} secondLeftIconName="menu" secondLeftIconAction={() => NavigatorService.toggle()} />     
                <View style={{ flex: 1 }}>
                    {instance().MAlerts.noAlerts.length > 0 ?
                        this._renderList() :
                        this._renderEmptyList()
                    }
                </View>
            </View>
        )
    }
}

export default MapaLista;

const styles = StyleSheet.create({

    swipeMenu: {
        flexDirection: 'column',
        padding: 15
    },
    swipeBack: {
        margin: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        alignItems: 'center',
        borderRadius: 10,
    },
    swipeBack2: {
        borderRadius: 10,
        flexDirection: 'row'
        , justifyContent: 'space-between', flex: 1, alignItems: 'center', height: '100%',
    },
    swipeLeft:{ 
        backgroundColor: colors.primaryColor, 
        flex: 1, flexDirection: 'row', 
        height: '100%', alignItems: 'center', 
        borderTopLeftRadius: 10, borderBottomLeftRadius: 10 
    },
    swipeRight:{ 
        backgroundColor: colors.secondaryColor, 
        flex: 1, flexDirection: 'row', height: '100%', 
        alignItems: 'center', justifyContent: 'flex-end', 
        borderTopRightRadius: 10, borderBottomRightRadius: 10 
    }



});

