import React from 'react';
import {
    ActivityIndicator,
    View,
    Alert
} from 'react-native';
import navigatorService from '../../navigation/navigatorService';
import { instance } from '../../models'

import colors from '../../style/colors'

class MyAlertLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        this._chargeInformation();
    }

    _chargeInformation() {
        instance().MAlerts.getAlertById().then((response:any[]) => {
            if (response) {
                let alertID=instance().MAlerts.alertIDSelected
                instance().MTracks.getTracksByAlertId(instance().MAlerts.alertIDSelected).then((response:any) => {
                    if (response) {
                        instance().MMessages.getMessagesById(instance().MAlerts.alertIDSelected).then((response:any) => {
                            if (response) {
                                instance().MZones.getZonesByAlertId(alertID).then((response)=>
                                {
                                    if (response){
                                        instance().MMessages.repartirMessages(instance().MLogin.username)
                                        if (instance().MAlerts.myAlert) navigatorService.navigate('MiAlertStack')
                                        else navigatorService.navigate('MiNoAlertStack')
                                    }else {
                                        Alert.alert("Un error en la base de datos")
                                        navigatorService.back()
                                    }

                                })
                              
                            } else {
                                Alert.alert("Un error en la base de datos")
                                navigatorService.back()
                            }
                        })
                    } else {
                        Alert.alert("Un error en la base de datos")
                        navigatorService.back()
                    }
                })
            } else {
                Alert.alert("Un error en la base de datos")
                navigatorService.back()
            }
        })

    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator size="large" color={colors.primaryColor}>
                </ActivityIndicator>
            </View>
        )
    }
}
export default MyAlertLoadingScreen