import React from 'react';
import {
    View,
    StyleSheet,
    YellowBox,
    Alert,
} from 'react-native';
import MapView, { Polyline,Polygon, Marker, AnimatedRegion, Circle, PROVIDER_GOOGLE } from 'react-native-maps';
import IconButton from '../../components/iconButton';
import { instance } from '../../models'
import { toJS } from 'mobx';
import Geolocation from '@react-native-community/geolocation';
import { Icon } from 'react-native-elements';
import { observer } from 'mobx-react';

var colors = ['#B266FF', 'green', 'blue', 'orange', 'yellow'];
var interval;

@observer
export default class MyAlertMap extends React.Component {


    constructor(props) {
        super(props)
        YellowBox.ignoreWarnings(['ViewPagerAndroid']);
        YellowBox.ignoreWarnings([
            'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?'
          ])
        this.state = {
            myTrack: [],
            latitude: 0,
            longitude: 0,
            error: null,
            coords: [],
            x: 'false',
        };
    }

    
    _updateMyTrackFake() {
        var longitude = instance().MTracks.longitude + 0.01
        var latitude = instance().MTracks.latitude + 0.01
        var myTrack = instance().MTracks.myTrack

        myTrack.push({
            "latitude": latitude,
            "longitude": longitude,
        })
        this.setState({
            latitude: latitude,
            longitude: longitude,
            error: null,
        });

        instance().MTracks.setMyTrack(myTrack)
        instance().MTracks.setLatitude(latitude)
        instance().MTracks.setLongitude(longitude)
    }

  
    _recordingMyTrackFake() {
        this._updateMyTrackFake()

    }

 

    _startTrackingFake() {
        interval = setInterval(() => {
            this._recordingMyTrackFake()
        }, 5000);

        var myTrack = instance().MTracks.myTrack
        myTrack.push({
            "latitude": instance().MAlerts.alertSelected[0].coord1,
            "longitude":instance().MAlerts.alertSelected[0].coord2,
        })
        
        instance().MTracks.setMyTrack(myTrack)
       

        //   setInterval(() => this._recordingMyTrackFake, 1000)
        instance().MTracks.setTracking(true)
    }

    _stopTracking() {
        clearInterval(interval);
        instance().MTracks.postTrack(instance().MAlerts.alertIDSelected).then((resuls) => {
            if (resuls) Alert.alert("Track guardado")
        })
    }


    componentDidMount() {
        Geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: instance().MAlerts.alertSelected[0].coord1,
                    longitude: instance().MAlerts.alertSelected[0].coord2,
                    error: null,
                });
                instance().MTracks.setLatitude(instance().MAlerts.alertSelected[0].coord1)
                instance().MTracks.setLongitude(instance().MAlerts.alertSelected[0].coord2)
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );
    }

    componentWillUnmount() {
        clearInterval(interval);
    }

    _renderMyTrack(myTrack: any) {
        return (
            <Polyline
                coordinates={toJS(myTrack)}
                strokeColor='red' // fallback for when `strokeColors` is not supported by the map-provider
                strokeWidth={4}
            />
        )
    }

    _renderTracks(item: any, index: number) {
        return (
            <Polyline
                key={index}
                coordinates={toJS(item.coordinates)}
                strokeColor={colors[index]} // fallback for when `strokeColors` is not supported by the map-provider
                strokeWidth={2}
            />
        )
    }

    _renderPuntos(punto: any,index:number) {
        console.log("puntooo",punto);
        let coordinate={
            latitude: punto[0],
            longitude:punto[1],
          }
         return (
            <Marker key={index} 
            coordinate={coordinate}
            description={"Punto Socorro "+index}
            title={"SOS"}
            >
          </Marker>
         )
    }

    _renderZones(item:any,index:number){
        console.log("item render zones",toJS(item.coordinates))
        var arrayLatLng=[]
        for(let i=0;i<item.coordinates.length;i++){
            var latlng={
                latitude:item.coordinates[i][0],
                longitude:item.coordinates[i][1]
            }
            arrayLatLng.push(latlng)
        }
         return (
            <Polygon
                key={index}
                coordinates={arrayLatLng}
                fillColor={'rgba(255, 252, 187, 0.5)'}
                strokeColor="#FFF851" // fallback for when `strokeColors` is not supported by the map-provider
                strokeWidth={2}
            />
        )
    }



    render() {
        var datosAlerta = instance().MAlerts.alertSelected[0]
        var tracksAlerta = instance().MTracks.tracks
        var messages = instance().MMessages.messages;
        console.log("messages from this alert",toJS(messages))
       
        console.log("tracksAlertaaaa",toJS(tracksAlerta))
        return (
            <View style={{ flex: 1 }}>
                <MapView style={styles.map} initialRegion={{
                    latitude: datosAlerta.coord1,
                    longitude: datosAlerta.coord2,
                    latitudeDelta: 0.15,
                    longitudeDelta: 0.15,
                }}>
                    {instance().MTracks.tracks.map((item, index) => this._renderTracks(item, index))}
                    {instance().MZones.zonesFromAlert.map((item, index) => this._renderZones(item, index))}
                    {instance().MAlerts.alertSelected[0].puntosImportantes &&
                    instance().MAlerts.alertSelected[0].puntosImportantes.map((item, index) => this._renderPuntos(item, index))}
                    {this._renderMyTrack(instance().MTracks.myTrack)}
                    <Circle
                        center={{
                            latitude: datosAlerta.coord1,
                            longitude: datosAlerta.coord2,
                        }}
                        radius={5000}
                        fillColor="rgba(204, 153, 255, 0.4)"
                        strokeColor="rgba(204, 153, 255, 0.3)"
                        zIndex={2}
                        strokeWidth={2}
                    />
                     <Marker
                            coordinate={{
                                "latitude": this.state.latitude,
                                "longitude":  this.state.longitude,
                            }}
                            title={"Your Location"}
                            description={"Your Location"}
                        >
                            <Icon name="map-pin" type='font-awesome' size={30} color='blue' />

                        </Marker>
                    {/* {!!this.state.latitude && !!this.state.longitude &&
                        <Marker
                            coordinate={{
                                "latitude": this.state.latitude,
                                "longitude": this.state.longitude
                            }}
                            title={"Your Location"}
                            description={"Your Location"}
                        >
                            <Icon name="map-pin" type='font-awesome' size={20} color='blue' />

                        </Marker>} */}

                </MapView>

                {instance().MTracks.tracking && instance().MAlerts.myAlert ?
                    <IconButton iconName='stop'  iconAction={() => { this._stopTracking() }} />
                    :null}
                {!instance().MTracks.tracking && instance().MAlerts.myAlert ?
                    <IconButton iconName='circle' iconAction={() => { this._startTrackingFake() }} />
                :null}

            </View>
        )

    }


}



const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: "flex-end",
        alignItems: "center"
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    bubble: {
        flex: 1,
        backgroundColor: "rgba(255,255,255,0.7)",
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20
    },
    latlng: {
        width: 200,
        alignItems: "stretch"
    },
    button: {
        width: 80,
        paddingHorizontal: 12,
        alignItems: "center",
        marginHorizontal: 10
    },
    buttonContainer: {
        flexDirection: "row",
        marginVertical: 20,
        backgroundColor: "transparent"
    }
});



// _updateMyTrack(position:any) {
    //     var myTrack = this.state.myTrack;
    //     console.log("updateMytrakc",myTrack)
    //     myTrack.push({
    //         "latitude": position.coords.latitude,
    //         "longitude": position.coords.longitude,
    //     })
    //     this.setState({ myTrack: myTrack })
    // }
    
      // _recordingMyTrack() {
    //     Geolocation.getCurrentPosition((position) => {
    //         this._updateMyTrack(position)
    //     },
    //     );
    // }

       // _startTracking() {
    //     interval = setInterval(() => {
    //         this._recordingMyTrack()
    //       }, 5000);
    //     setInterval(() => this._recordingMyTrack, 1000)
    //     instance().MTracks.setTracking(true)
    // }

    // componentDidMount() {
    //     Geolocation.getCurrentPosition(
    //         (position) => {
    //             console.log(position);
    //             this.setState({
    //                 latitude: position.coords.latitude,
    //                 longitude: position.coords.longitude,
    //                 error: null,
    //             });
    //         },
    //         (error) => this.setState({ error: error.message }),
    //         { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
    //     );
    // }