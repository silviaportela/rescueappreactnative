import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';
import { instance } from '../../models'
import { toJS } from 'mobx'
import colors from '../../style/colors'
import { Icon } from 'react-native-elements';

export default class MyAlertInfo extends React.Component {

    constructor(props) {
        super(props)

    }

    _formatTime(time: number) {
        time = time / 1000
        var days: number = Math.floor(time / 86400);
        var hours: number = Math.floor((time % 86400) / 3600);
        var minutes: number = Math.floor((time % 3600) / 60);
        var seconds: number = time % 60;
        console.log(days)
        console.log(hours)
        console.log(minutes)
        console.log(seconds)
        //Anteponiendo un 0 a los minutos si son menos de 10 
        minutes = minutes < 10 ? 0 + minutes : minutes;

        //Anteponiendo un 0 a los segundos si son menos de 10 
        seconds = seconds < 10 ? 0 + seconds : seconds;

        var result = days + " días "
        result += hours + " horas " + minutes + " minutos";  // 2:41:30
        return result.toString();
    }

    render() {
        console.log("INFO", toJS(instance().MAlerts.alertSelected))
        var date = instance().MAlerts.alertSelected[0].dateDifference;
        return (
            <View style={styles.container}>
                <Text style={styles.capitalLetter}>{instance().MAlerts.alertSelected[0].name}</Text>

                <View style={{ flexDirection: 'row'}}>
                    <Icon name="map-pin" type='font-awesome' size={20} color={'red'} />
                    <Text> {instance().MAlerts.alertSelected[0].city}</Text>
                </View>

                <View style={{ flexDirection: 'row' , marginTop: 75 }}>
                    <Text style={styles.wordBold} >Estado:</Text>
                    <Text>{instance().MAlerts.alertSelected[0].status == 1 ? 'Activa' : 'No Activa'}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.wordBold}>Número de participantes: </Text>
                    <Text > {instance().MAlerts.alertSelected[0].count}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                <Text style={styles.wordBold}>Tiempo acumulado: </Text>
                <Text> {this._formatTime(date)}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginTop: 100,
        padding: 20
    },
    text: {
        color: '#41cdf4',
    },
    capitalLetter: {
        fontWeight: 'bold',
        color: colors.primaryColorSoft,
        fontSize: 25
    },
    wordBold: {
        fontWeight: 'bold',
        color: 'black'
    },
    italicText: {
        color: '#37859b',
        fontStyle: 'italic'
    },
    textShadow: {
        textShadowColor: 'red',
        textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 5
    }
})



