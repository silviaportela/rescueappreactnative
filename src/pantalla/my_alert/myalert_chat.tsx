
import React from 'react'
import {
    StyleSheet,
    TextInput,
    View,
    FlatList,
    ScrollView,
    TouchableHighlight
} from 'react-native'
import { Text, Icon } from 'react-native-elements';
import { instance } from '../../models'
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions'

import { observer } from 'mobx-react';
import { toJS } from 'mobx';
@observer
export default class MyAlertChat extends React.Component {


    _onSend(message:String) {
        var d = new Date();
        var data = {
            user: instance().MLogin.username,
            idAlert: instance().MAlerts.alertIDSelected,
            timestamp: d.getTime(),
            message: message
        }

        instance().MMessages.postMessage(data)
        instance().MSocket.sendMensaje(data)

    }

    formatAMPM(date) {
        console.log(date.getHours())
        let strTime = date.getHours() + ":" + date.getMinutes()
        strTime = strTime + " (" + date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + ")";
        return strTime;
    }
  
    _renderRow(item: any, index: number) {
        let hora= new Date(item.timestamp*1)
        let horaFormateada=this.formatAMPM(hora)
        return (
            <View key={index} style={[styles.messageRow, item.user == instance().MLogin.username && styles.meRow]}>
              <TouchableHighlight
                underlayColor='#dddddd'
                style={[styles.messageContent, item.user == instance().MLogin.username && styles.me]}>
                  <View>
                      <Text style={styles.message}>{item.message}</Text>
                  </View>
              </TouchableHighlight>
              <Text style={styles.messageDate}>{item.user}</Text>
              <Text style={styles.messageDate}>{horaFormateada}</Text>
            </View>
          );
       
    }


    keyExtractor = (item, index) => index.toString()

    _renderItem(item) {
        return (
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingVertical: responsiveHeight(1),
                marginHorizontal: responsiveWidth(3)
            }}>
                <Icon reverse name="key" type='font-awesome' size={20} />
                <Text style={{
                    fontSize: responsiveFontSize(2.3),
                    marginLeft: responsiveWidth(2)
                }}>{item.user}: </Text>
                <Text style={{
                    fontSize: responsiveFontSize(2.3),
                    marginLeft: responsiveWidth(2)
                }}>{item.message} </Text>
            </View>)
    }

    _renderList() {
        console.log("renderlinst")
        var array: any[] = toJS(instance().MMessages.messages);
        return (
            <View>
                {array.map((item, index) => this._renderRow(item,index))}
            </View>
        )
    }
    render() {
        console.log("instance(),messages", toJS(instance().MMessages.messages))
        var array: any[] = toJS(instance().MMessages.messages);

        return (
            <ScrollView
                automaticallyAdjustContentInsets={true}
                keyboardDismissMode="on-drag"
                keyboardShouldPersistTaps="never"
                showsVerticalScrollIndicator={false}
                ref='scrollView'
                contentContainerStyle={styles.container}
            >

                <View
                    style={{
                        flex: 2,
                        padding: 10,
                        // backgroundColor: 'green'
                    }}>
                    {array.length > 0 ?
                        this._renderList() : null
                    }
                </View>
                <TextInput

                    value={instance().MMessages.myMessage}
                    onSubmitEditing={() => this._onSend(instance().MMessages.myMessage)}
                    placeholder="Message..."
                    returnKeyType="send"
                    ref="newMessage"
                    onChangeText={(value) => instance().MMessages.setMyMessage(value)} />
            </ScrollView>
        );

    }
}


var styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 60,
       // backgroundColor: 'blue'
    },
    messageRow: {
        alignItems: 'flex-start',
        marginBottom: 5,
    },
    meRow: {
        alignItems: 'flex-end'
    },
    messageContent: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        padding: 10,
        borderRadius: 10,
        backgroundColor: '#ebebeb',
    },
    me: {
        alignItems: 'flex-end',
        backgroundColor: '#d2fffd',
    },
    message: {
        fontSize: 16,
        color: '#888'
    },
    messageDate: {
        fontSize: 12,
        color: '#656565',
        padding: 2,
    },
    text: {
        color: '#000',
    },
});