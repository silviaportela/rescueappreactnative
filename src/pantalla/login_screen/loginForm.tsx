import React, { Component } from 'react'
import { StyleSheet, View, Alert } from 'react-native'
import { Text } from 'react-native-animatable'
import { View as Aview } from 'react-native-animatable'
import { responsiveWidth } from 'react-native-responsive-dimensions'
import { Input } from 'react-native-elements';

import IconButton from '../../components/iconButton'
import CustomHeader from '../../components/customHeader';
import colors from '../../style/colors';
import strings from '../../style/strings';

import { instance } from '../../models'
import { observer } from 'mobx-react';



@observer
export default class LoginForm extends Component {

  _login() {
    if (instance().MLogin.username == '' || instance().MLogin.password == '')
      Alert.alert("Error", "Campos vacíos")
    else
      instance().MLogin.login()
  }

  hideForm = async () => {
    if (this.buttonRef && this.formRef && this.linkRef) {
      await Promise.all([
        this.buttonRef.zoomOut(200),
        this.formRef.fadeOut(300),
        this.linkRef.fadeOut(300)
      ])
    }
  }
  buttonRef: any;
  formRef: any;
  linkRef: any;
  emailInputRef: Input | null | undefined;
  //phoneInputRef: Input | null | undefined;
  passwordInputRef: Input | null | undefined;

  render() {
    return (
      <Aview style={styles.container}>
        <CustomHeader backButton={false} headerTitle={strings.signIn_header_title} logoutButton={false} />

        <View style={{ flex: 1 }}>
          <Aview ref={(ref) => { this.formRef = ref }}>
            <Input
              placeholderTextColor={colors.primaryColorSoft}
              ref={(ref) => this.emailInputRef = ref}
              placeholder={strings.email_placeHolder}
              keyboardType={'email-address'}
              editable={true}
              returnKeyType={'next'}
              blurOnSubmit={false}
              onSubmitEditing={() => this.passwordInputRef.focus()}
              value={instance().MLogin.username}
              onChangeText={(value) => instance().MLogin.setUsername(value)}
            />
            <Input
              placeholderTextColor={colors.primaryColorSoft}
              ref={(ref) => this.passwordInputRef = ref}
              placeholder={strings.password_placeHolder}
              editable={true}
              returnKeyType={'next'}
              secureTextEntry={true}
              value={instance().MLogin.password}
              onChangeText={(value) => instance().MLogin.setPassword(value)}
            />
          </Aview>
          <Aview style={styles.footer}>
            <Text
              ref={(ref) => this.linkRef = ref}
              style={styles.loginLink}
              onPress={() => {
                instance().MLogin.setRegisterForm(true);
                instance().MLogin.setLoginForm(false);
              }}
              animation={'fadeIn'}
              duration={600}
              delay={400}
            >
              ¿Aun no tienes cuenta?
              </Text>
            <IconButton iconName='play' iconAction={() => this._login()} />

          </Aview>
        </View>
      </Aview>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },

  footer: {
    height: 100,
    width: responsiveWidth(100),
    position: 'absolute',
    justifyContent: 'space-between',
    bottom: 0,
    flexDirection: 'row',
  },
  signupLink: {
    color: colors.primaryColor,
    alignSelf: 'center',
    padding: 20
  },
  loginLink: {
    color: colors.primaryColor,
    alignSelf: 'center',
    padding: 20
  }
})
