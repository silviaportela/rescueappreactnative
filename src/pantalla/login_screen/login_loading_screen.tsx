import React from 'react';
import {
    ActivityIndicator,
    View,
    Alert,
} from 'react-native';

import colors from '../../style/colors'

class LoginLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
       this._autologin();

    }
     _autologin() {
         this.props.navigation.navigate('login_screen')
    }
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator size="large" color={colors.primaryColor}>
                </ActivityIndicator>
            </View>
        )
    }
}
export default LoginLoadingScreen