import React, { Component } from 'react'
import { StyleSheet, View, ActivityIndicator, Alert } from 'react-native'
import { Text } from 'react-native-animatable'
import { View as AView } from 'react-native-animatable'
import { Input, Icon } from 'react-native-elements';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions'
import CustomHeader from '../../components/customHeader';
import colors from '../../style/colors'
import strings from '../../style/strings'
import { instance } from '../../models'
import { observer } from 'mobx-react';
import IconButton from '../../components/iconButton'
import navigatorService from '../../navigation/navigatorService';

@observer
export default class SignupForm extends Component {
  constructor(props) {
    super(props);
  }


  hideForm = async () => {
    if (this.buttonRef && this.formRef && this.linkRef) {
      await Promise.all([
        this.buttonRef.zoomOut(200),
        this.formRef.fadeOut(300),
        this.linkRef.fadeOut(300)
      ])
    }
  }
  buttonRef: any;
  formRef: any;
  linkRef: any;
  usernameInputRef: Input | null | undefined;
  passwordInputRef: Input | null | undefined;
  passwordConfirmInputRef: any;

  _controlPassword() {

    const isValid = instance().MLogin.username !== '' && instance().MLogin.password !== '' && instance().MLogin.passwordConfirm

    if (isValid) {
      if (instance().MLogin.password == instance().MLogin.passwordConfirm) {
        instance().MLogin.signUp().then((response)=>{
          if (response==true) navigatorService.navigate('Drawer')
        })
      }
      else {
        Alert.alert('Las contraseñas deben ser iguales');
      }
    }

  }

  render() {


    return (
      <AView style={styles.container}>
        <CustomHeader backButton={false} headerTitle={strings.register_user_header_title} logoutButton={false} />


        <View style={{ flex: 1 }}>
          <AView ref={(ref) => this.formRef = ref}>
            <Input
              placeholderTextColor={colors.primaryColorSoft}
              ref={(ref) => this.phoneInputRef = ref}
              placeholder={strings.email_placeHolder}
              keyboardType={'email-address'}
              returnKeyType={'next'}
              onSubmitEditing={() => this.passwordConfirmInputRef.focus()}
              value={instance().MLogin.username}
              onChangeText={(value) => instance().MLogin.setUsername(value)}
            />
            <Input
              placeholderTextColor={colors.primaryColorSoft}
              ref={(ref) => this.passwordInputRef = ref}
              placeholder={strings.password_placeHolder}
              returnKeyType={'next'}
              secureTextEntry={true}
              onSubmitEditing={() => this.passwordConfirmInputRef.focus()}
              value={instance().MLogin.password}
              onChangeText={(value) => instance().MLogin.setPassword(value)}
            />
            <Input
              placeholderTextColor={colors.primaryColorSoft}
              ref={(ref) => this.passwordConfirmInputRef = ref}
              placeholder={strings.password_placeHolder_confirmation}
              returnKeyType={'done'}
              secureTextEntry={true}
              value={instance().MLogin.passwordConfirm}
              onChangeText={(value) => instance().MLogin.setPasswordConfirm(value)}
            />
          </AView>
          <AView style={styles.footer}>
            <Text
              ref={(ref) => this.linkRef = ref}
              style={styles.loginLink}
              onPress={() => {
                instance().MLogin.setRegisterForm(false);
                instance().MLogin.setLoginForm(true);
              }}
              animation={'fadeIn'}
              duration={600}
              delay={400}
            >
              ¿Ya tienes cuenta?
              </Text>
            <IconButton

              iconName='play'
              iconAction={() => this._controlPassword()}
            />
          </AView>
        </View>

      </AView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: responsiveHeight(100),
    width: responsiveWidth(100),
    backgroundColor: 'white'
  },
  footer: {
    height: 100,
    width: responsiveWidth(100),
    position: 'absolute',
    justifyContent: 'space-between',
    bottom: 0,
    flexDirection: 'row',
  },
  createAccountButton: {
    backgroundColor: 'white'
  },
  createAccountButtonText: {
    color: '#3E464D',
    fontWeight: 'bold'
  },
  loginLink: {
    color: colors.primaryColor,
    alignSelf: 'center',
    padding: 20
  }
})
