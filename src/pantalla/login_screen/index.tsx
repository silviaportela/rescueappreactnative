
import React from 'react';
import { View,Text } from 'react-native-animatable'
import { StyleSheet,YellowBox } from 'react-native'
import { observer } from 'mobx-react';
import strings from '../../style/strings'
import colors from '../../style/colors'
import CustomFilledButton from '../../components/customFilledButton';
import CustomBorderButton from '../../components/customBorderButton';
import {instance} from '../../models'
import LoginForm from '../login_screen/loginForm'
import SignupForm from '../login_screen/SignupForm'
import navigatorService from '../../navigation/navigatorService';
import dataInterface from '../../dataInterface';
import { getUserProfile } from '../../dataInterface/cloudDataInterface';
@observer
class LoginScreen extends React.Component {

    constructor(props:any) {
        super(props)
        YellowBox.ignoreWarnings(['ViewPagerAndroid']);
        this._loading();
    }

    _login(){
        instance().MLogin.setLoginForm(false);
        instance().MLogin.setRegisterForm(true);
      
    }
    _signup(){
        instance().MLogin.setRegisterForm(false);
        instance().MLogin.setLoginForm(true);
    }
    _loading() {
        dataInterface.getUserProfile().then((profile: any) => {
            console.log("loading",profile)
            console.log("loading",profile==null)
            if (profile!==null){
                instance().MLogin.setUsername(profile)
                navigatorService.navigate('Drawer')
            }
            else{
                console.log("ES PUTO NULL")
            }
               
            
            })
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                {instance().MLogin.loginForm? 
                    <LoginForm/>
                :
                null}
                {instance().MLogin.registerForm ? 
                   <SignupForm/>
                :
                null}
                {!instance().MLogin.loginForm && !instance().MLogin.registerForm ? 
                <View style={styles.container}>
                    <View animation={'zoomIn'} delay={600} duration={400} style={{ padding: 10 }}>
                        <CustomFilledButton
                            title={strings.createAccount}
                            onPress={()=> this._login()}
                            buttonColor='white'
                            textColor={colors.primaryColor}
                        />
                    </View>
                    <View animation={'zoomIn'} delay={800} duration={400} style={{ padding: 10 }}>
                        <CustomBorderButton
                            title={strings.signIn}
                            onPress={()=>{
                              this._signup()
                            }}
                            buttonColor={colors.primaryColor}
                            textColor='white'
                        />
                    </View>
                </View>
                :null }
            </View>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: colors.primaryColor,
    },
})

export default LoginScreen


