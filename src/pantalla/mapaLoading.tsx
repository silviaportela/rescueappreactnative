import React from 'react';
import {
  View,
  ActivityIndicator, Alert
} from 'react-native';
import { instance } from '../models'
import navigatorService from '../navigation/navigatorService';
import { observer } from 'mobx-react';
import dataInterface from '../dataInterface';

@observer
export default class MapaLoading extends React.Component {

  constructor(props) {
    super(props);

    this._chargeInformation();

  }

  _chargeInformation() {

    var username = instance().MLogin.username
    dataInterface.getUserProfile().then((username: any) => {
      instance().MLogin.setUsername(username)
      instance().MAlerts.getAlerts(username).then((response: any) => {
      if (response) {
        instance().MAlerts.getNoAlerts(username).then((response2: any) => {
          if (response2 && instance().MAlerts.permission) {
            navigatorService.navigate('MapaView')
          } else {
            Alert.alert("Un error en la base de datos")
            navigatorService.back()
          }
        })
      } else {
        Alert.alert("Un error en la base de datos")
        navigatorService.back()
      }
    });
  })
  }

  render() {
    console.log("mapaloading")
    return (
      <View style={{ flex: 1 }}>
        <ActivityIndicator></ActivityIndicator>
      </View>
    )

  }

}

