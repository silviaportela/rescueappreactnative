import React from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Alert
} from 'react-native'

import { Icon } from 'react-native-elements'
import colors from '../style/colors';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions'
import navigatorService from '../navigation/navigatorService'
import strings from '../style/strings'
import { instance } from '../models';
import { observer } from 'mobx-react';
import dataInterface from '../dataInterface';


@observer
class DrawerMenuScreen extends React.Component {

    constructor(props) {
        super(props)
    }

    _logoutAlert() {
        Alert.alert(
            "Seguro?",
            strings.logout_msg,
            [
                {
                    text: 'LOGOUT', onPress: () => {
                        instance().MLogin.logOut().then((response) => {

                            dataInterface.resetUserProfile().then((response) => {
                            
                                navigatorService.navigate('LoginStack')
                            });

                        })

                    }
                },
                { text: 'CANCEL', onPress: () => console.log('CANCEL sakatua') }
            ],
            { cancelable: true }
        )
    }

    _renderIcon(name: string, reverse: boolean) {
        return (
            <Icon
                name={name}
                type='font-awesome'
                reverse={reverse}
                size={responsiveHeight(2.5)}
                color={colors.secondaryBlueColor} />
        )
    }

    _renderTouchable(title: string, action: Function, nameIcon: string) {
        return (
            <TouchableOpacity style={styles.touchableOpacity2} onPress={() => action()}>
                {this._renderIcon(nameIcon, false)}
                <Text style={{ fontSize: responsiveFontSize(1.5), paddingLeft: 10, color: 'white', fontWeight: 'bold' }}>{title}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'column', flex: 1, justifyContent: 'center', backgroundColor: colors.thirdBlueColor }}>
                    <TouchableOpacity style={styles.touchableOpacity} onPress={() => { navigatorService.navigate('MyProfile') }}>
                        <View >
                            {this._renderIcon("user", true)}
                        </View>
                        <View style={{ flexDirection: 'column', alignContent: 'center' }}>
                            <Text style={{ color: 'white', fontSize: responsiveFontSize(2.5) }}>
                                {/* {instance().user.name ? instance().user.name : '(sin nombre)'} */}
                                {instance().MLogin.username}
                            </Text>
                            <Text style={{ color: 'white', fontSize: responsiveFontSize(1.5) }}>
                                {/* {instance().user.adress ? instance().user.adress : '(sin direccion)'} */}
                                Direccion
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'column', flex: 9, backgroundColor: colors.primaryColor }}>
                    {this._renderTouchable("Mis Alertas",
                        () => { navigatorService.navigate('MapaView') }, "map")}
                    {this._renderTouchable("Todas las alertas",
                        () => { navigatorService.navigate('MapaLista') }, "list")}
                </View>

                <View style={{ flexDirection: 'column', flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: colors.thirdBlueColor }}>
                    <TouchableOpacity onPress={() => this._logoutAlert()}>
                        <Text style={{ fontSize: responsiveFontSize(2.5), color: 'white', }}>
                            Log Out
                        </Text>
                    </TouchableOpacity>
                </View>
            </View >

        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    touchableOpacity: {
        flexDirection: 'row',
        marginLeft: 50,
        marginTop: 10,
    },
    touchableOpacity2: {
        flexDirection: 'row',
        marginLeft: 50,
        marginTop: 20,
    }


});


export default DrawerMenuScreen
