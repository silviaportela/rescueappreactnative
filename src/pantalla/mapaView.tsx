import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import CustomHeader from '../components/customHeader'
import NavigatorService from '../navigation/navigatorService';

import { instance } from '../models'
import { observer } from 'mobx-react';

@observer
class MapaView extends React.Component {

  constructor(props) {
    super(props);
  }


  _goToAlert(id:String){
    instance().MAlerts.setAlertIDSelected(id)
    instance().MAlerts.setMyAlert(true)
    NavigatorService.navigate('MyAlertSwitch')
}

  _renderItem(item:any, index:number) {
    let coordinate={
      latitude: item.coord1,
      longitude: item.coord2,
    }
    return (
      <Marker key={index} 
        coordinate={coordinate}
        description={item.name}
        title={item.name}
        onCalloutPress={()=>this._goToAlert(item._id)}
        >
      </Marker>
    )
  }

  _renderAlerts() {
    return (
      instance().MAlerts.alerts.map((item:any, index:number) =>
        this._renderItem(item, index))
    )
  }

  render() {
    return (
      <View style={{flex:1}}>
          <CustomHeader backButton={false} headerTitle="Rescue App" logoutButton={false} secondLeftIconName="menu" secondLeftIconAction={() => NavigatorService.toggle()} />
               
      <View style={styles.container}>
        <MapView
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={styles.map}
          region={{
            latitude: 43.166667,
            longitude: -2.45,
            latitudeDelta: 1,
            longitudeDelta: 1,
          }}
        >
          {this._renderAlerts()}
        </MapView>
        </View>
      </View>
    )

  }

}

export default MapaView;


const styles = StyleSheet.create({
  container: {
    flex:1,
    height: 800,
    width: 400,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});