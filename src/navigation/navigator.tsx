//Navigators
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import {YellowBox} from 'react-native'
import NavigatorService from './navigatorService'
import { View, Button, Animated, Easing } from 'react-native'

import loading_login_screen from '../pantalla/login_screen/login_loading_screen'
import login_screen from '../pantalla/login_screen/index'
import React from 'react';

import mapa_loading from '../pantalla/mapaLoading'
import mapa_view from '../pantalla/mapaView'
import mapa_lista from '../pantalla/mapaLista'

import myalert_loading from '../pantalla/my_alert/myalert_loading'
import myalert_map from '../pantalla/my_alert/myalert_mapa'
import myalert_chat from '../pantalla/my_alert/myalert_chat'
import myalert_info from '../pantalla/my_alert/myalert_info'

import DrawerMenuScreen from '../pantalla/drawen_menu_screen';

import { Icon } from 'react-native-elements';
import colors from '../style/colors'
import CustomHeader from '../components/customHeader'
//Navigationservice

const MyNoAlertTab = createBottomTabNavigator({

  MyAlertMap: {
    screen: myalert_map,
    navigationOptions: {
      tabBarIcon: (value: { focused: boolean }) => {
        return <Icon name='map'
          type='font-awesome'
          size={25} color={value.focused ? 'white' : colors.thirdBlueColor} />
      },
    }
  },
  
  MyAlertInfo: {
    screen: myalert_info,
    navigationOptions: {
      tabBarIcon: (value: { focused: boolean }) => {
        return <Icon
          name='info'
          type='font-awesome'
          size={25} color={value.focused ? 'white' : colors.thirdBlueColor} />
      },
    }
  },
}, {
  tabBarOptions: {
    showLabel: false,
    activeTintColor: 'white',
    style: {
      backgroundColor: colors.secondaryBlueColor // TabBar background
    },


  },
  navigationOptions: {
    headerTitle: <CustomHeader backButton={true} headerTitle="Detalle Alerta" logoutButton={false} color={true} />,
  },
  initialRouteName: 'MyAlertMap',
  backBehavior: 'none'

});

const MyAlertTab = createBottomTabNavigator({

  MyAlertMap: {
    screen: myalert_map,
    navigationOptions: {
      tabBarIcon: (value: { focused: boolean }) => {
        return <Icon name='map'
          type='font-awesome'
          size={25} color={value.focused ? 'white' : colors.thirdBlueColor} />
      },
    }
  },
  MyAlertChat: {
    screen: myalert_chat,
    navigationOptions: {
      tabBarIcon: (value: { focused: boolean }) => {
        return <Icon name='comments'
          type='font-awesome'
          size={25} color={value.focused ? 'white' : colors.thirdBlueColor} />
      },
    }
  },

  MyAlertInfo: {
    screen: myalert_info,
    navigationOptions: {
      tabBarIcon: (value: { focused: boolean }) => {
        return <Icon
          name='info'
          type='font-awesome'
          size={25} color={value.focused ? 'white' : colors.thirdBlueColor} />
      },
    }
  },
}, {
  tabBarOptions: {
    showLabel: false,
    activeTintColor: 'white',
    style: {
      backgroundColor: colors.secondaryBlueColor // TabBar background
    },


  },
  navigationOptions: {
    headerTitle: <CustomHeader backButton={true} headerTitle="Detalle Alerta" logoutButton={false} color={true} />,
  },
  initialRouteName: 'MyAlertMap',
  backBehavior: 'none'

});


const MiAlertStack = createStackNavigator({
  MyAlertTab: MyAlertTab,
}, {
  headerMode: 'float',
},
);

const MiNoAlertStack = createStackNavigator({
  MyNoAlertTab: MyNoAlertTab,
}, {
  headerMode: 'float',
},
);

const MyAlertSwitch = createSwitchNavigator({
  my_alert_loading_screen: { screen: myalert_loading, },
  MiAlertStack: MiAlertStack,
}, {
  initialRouteName: 'my_alert_loading_screen',
});

const MyNoAlertSwitch = createSwitchNavigator({
  my_alert_loading_screen: { screen: myalert_loading, },
  MiNoAlertStack: MiNoAlertStack,
}, {
  initialRouteName: 'my_alert_loading_screen',
});


const MapSwitch = createSwitchNavigator({
  MapaLoading: { screen: mapa_loading },
  MapaView: {screen: mapa_view},
}, {
  initialRouteName: 'MapaLoading',
});

//Mapa principal + mi alerta
const MapStack = createStackNavigator({
  MapSwitch: MapSwitch,
  MyAlertSwitch: MyAlertSwitch,
  MyNoAlertSwitch: MyNoAlertSwitch
}, {
  headerMode: 'none',
  initialRouteName: 'MapSwitch',

});

const Drawer = createDrawerNavigator({
  MapStack: MapStack,
  MapaLista: {screen:mapa_lista},
}, {
  contentComponent: props => (<DrawerMenuScreen {...props} />),
}

);

const LoginStack = createSwitchNavigator({
  loading_login_screen: { screen: loading_login_screen, },
  login_screen: { screen: login_screen },
}, {
  initialRouteName: 'loading_login_screen',
});

const InitialNavigator = createSwitchNavigator({
  LoginStack: LoginStack,
  Drawer: Drawer,
},
  {
    initialRouteName: 'LoginStack',
  }
)


const App = createAppContainer(InitialNavigator);

export default class Stack extends React.Component {
 
  constructor(props) {
    super(props)
    YellowBox.ignoreWarnings(['ViewPagerAndroid']);
    YellowBox.ignoreWarnings(['Warning: Async Storage has been extracted from react-native core']);
    
  }
  render() {
    
    return (
      <View style={{ flex: 1 }}>
        <App
          ref={navigatorRef => {
            NavigatorService.setContainer(navigatorRef);
          }}
        />
      </View>
    );
  }
}

  // export default App;



